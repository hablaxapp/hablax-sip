Add PushKit.framework
Add Intents.framework
Enable Voice over IP


Put this lines in AppDelegate.m

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    PKPushRegistry *pushRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    pushRegistry.delegate = self;
    pushRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
    
    self.viewController = [[MainViewController alloc] init];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(nonnull NSUserActivity *)userActivity restorationHandler:(nonnull void (^)(NSArray * _Nullable))restorationHandler {
    if ([userActivity.interaction.intent isKindOfClass:[INStartAudioCallIntent class]]) {
        INPerson *person = [[(INStartAudioCallIntent*)userActivity.interaction.intent contacts] firstObject];
        NSString *phoneNumber = person.personHandle.value;
//        CallViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"CallViewController"];
//        viewController.phoneNumber = phoneNumber;
//        UIViewController *mainViewController = self.window.rootViewController;
//        [mainViewController presentViewController:viewController animated:YES completion:nil];
    }
    return YES;
}

#pragma mark - PKPushRegistryDelegate

- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type {
    if([credentials.token length] == 0) {
        NSLog(@"voip token NULL");
        return;
    }
}

- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(NSString *)type {
    NSString *uuidString = payload.dictionaryPayload[@"UUID"];
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:uuidString];
    NSString *phoneNumber = payload.dictionaryPayload[@"PhoneNumber"];
//    CallViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"CallViewController"];
//    viewController.phoneNumber = phoneNumber;
//    viewController.isIncoming = YES;
//    viewController.uuid = uuid;
//    UIViewController *mainViewController = self.window.rootViewController;
//    [mainViewController presentViewController:viewController animated:YES completion:nil];
}