#import <Cordova/CDV.h>
//#import "RestCommClient.h"
#import "SipManager.h"
#import "CallManager.h"

typedef enum {
    AccountStateOffline = 0,  /**< Device is offline */
    AccountStateReady,  /**< Device is ready to make and receive connections */
    AccountStateBusy  /**< Device is busy */
} AccountState;

typedef enum
{
    ConnectionStatePending = 0,  /**< Connection is in state pending */
    ConnectionStateConnecting,  /**< Connection is in state connecting */
    ConnectionStateConnected,  /**< Connection is in state connected */
    ConnectionStateDisconnected  /**< Connection is in state disconnected */
} ConnectionState;

//RCDeviceDelegate, RCConnectionDelegate,
@interface LinphonePlugin : CDVPlugin<CallManagerDelegate, SipManagerDeviceDelegate, SipManagerConnectionDelegate>

//@property (nonatomic,retain) RCDevice* device;
//@property (nonatomic,retain) RCConnection* connection;
@property NSString * signalingUsername;
@property NSString * signalingDomain;
@property SipManager * sipManager;
@property AccountState state;
@property ConnectionState connectionState;

@property NSMutableDictionary * parameters;
@property BOOL isInitialized;
@property BOOL isRegistered;
@property BOOL cancelPending;
@property BOOL isTerminatedByCaller;
@property BOOL speakerEnabled;
@property BOOL bluetoothAvailable;
@property BOOL bluetoothEnabled;

@property NSString * callUsername;
@property NSString * callDomain;
@property BOOL makeCall;



- (void) keepAwake:(CDVInvokedUrlCommand*)command;
- (void) allowSleepAgain:(CDVInvokedUrlCommand*)command;
- (void) initLinphoneCore:(CDVInvokedUrlCommand*)command;
- (void) createMediaList:(CDVInvokedUrlCommand*)command;
- (void)mediaAtOneLeastReady:(CDVInvokedUrlCommand*)command;
- (void) destroyLinphoneCore:(CDVInvokedUrlCommand*)command;
- (void) registerSIP:(CDVInvokedUrlCommand*)command;
- (void) deregisterSIP:(CDVInvokedUrlCommand*)command;
- (void) getRegisterStatusSIP:(CDVInvokedUrlCommand*)command;
- (void) makeCall:(CDVInvokedUrlCommand*)command;
- (void) acceptCall:(CDVInvokedUrlCommand*)command;
- (void) declineCall:(CDVInvokedUrlCommand*)command;
- (void) stopListener:(CDVInvokedUrlCommand*)command;
- (void) startListener:(CDVInvokedUrlCommand*)command;


#pragma mark - Obtaining information about a running call: sound volumes, quality indicators Functions
- (void) terminateCall:(CDVInvokedUrlCommand*)command;
- (void) sendDtmf:(CDVInvokedUrlCommand*)command;
- (void) sendDtmfWithCallId:(CDVInvokedUrlCommand*)command;
- (void) muteCall:(CDVInvokedUrlCommand*)command;
- (void) unmuteCall:(CDVInvokedUrlCommand*)command;
- (void) enableSpeaker:(CDVInvokedUrlCommand*)command;
- (void) disableSpeaker:(CDVInvokedUrlCommand*)command;
- (void) holdCall:(CDVInvokedUrlCommand*)command;
- (void) holdCallWithCallId:(CDVInvokedUrlCommand*)command;
- (void) unholdCall:(CDVInvokedUrlCommand*)command;
- (void) unholdCallWithCallId:(CDVInvokedUrlCommand*)command;


- (void) getCallInformation:(CDVInvokedUrlCommand*)command;
- (void) sendMessageWithChatRoom:(CDVInvokedUrlCommand*)command;

@end
