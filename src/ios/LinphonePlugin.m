#import "LinphonePlugin.h"
#import <AudioToolbox/AudioToolbox.h>




@implementation NSString (md5)

- (BOOL)containsSubstring:(NSString *)str {
    
    if (UIDevice.currentDevice.systemVersion.doubleValue >= 8.0) {
#pragma deploymate push "ignored-api-availability"
        return [self containsString:str];
#pragma deploymate pop
    }
    return ([self rangeOfString:str].location != NSNotFound);
}

@end

@implementation LinphonePlugin

NSString* listenCallbackId = @"";
NSString* defaultUsername = @"";
NSString* defaultDomain = @"";
NSString* defaultPassword = @"";

- (void)keepAwake:(CDVInvokedUrlCommand*)command {
    NSString *callbackId = command.callbackId;
    
    // Acquire a reference to the local UIApplication singleton
    UIApplication* app = [UIApplication sharedApplication];
    
    if (![app isIdleTimerDisabled]) {
        [app setIdleTimerDisabled:true];
    }
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
}

- (void)allowSleepAgain:(CDVInvokedUrlCommand*)command {
    NSString *callbackId = command.callbackId;
    
    // Acquire a reference to the local UIApplication singleton
    UIApplication* app = [UIApplication sharedApplication];
    
    if([app isIdleTimerDisabled]) {
        [app setIdleTimerDisabled:false];
    }
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
}

- (void)initLinphoneCore:(CDVInvokedUrlCommand*)command {
    self.state = AccountStateOffline;
    [SipManager emptyMediaList];
    [SipManager createMediaList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)createMediaList:(CDVInvokedUrlCommand*)command {
    [SipManager emptyMediaList];
    [SipManager createMediaList];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)mediaAtOneLeastReady:(CDVInvokedUrlCommand*)command {
    NSNumber *isReady = [NSNumber numberWithBool:[SipManager mediaAtOneLeastReady]];
//    [SipManager emptyMediaList];
//    [SipManager createMediaList];
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       isReady, @"ready",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


- (void) destroyLinphoneCore:(CDVInvokedUrlCommand*)command {
    // [[LinphoneHelper instance] destroyLinphoneCore];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)callDidAnswer {
    // nothing
}
- (void)callDidEnd {
    // nothing
    
}
- (void)callDidHold:(BOOL)isOnHold {
    // nothing
}
- (void)callDidFail {
    // nothing
}

- (void)disconnect
{
    if (self.connectionState == ConnectionStateConnecting) {
        // for outgoing calls in state connecting (i.e. ringing), treat disconnect as cancel
        [self.sipManager cancel];
    }
    else if (self.connectionState == ConnectionStateConnected) {
        NSLog(@"[RCConnection disconnect:bye]");

        [self.sipManager bye];
        [self.sipManager cancel];
    }
    else if (self.connectionState == ConnectionStatePending) {
        // SIP doesn't support canceling before receiving provisional response (i.e. 180 Ringing, etc).
        // Let's mark for cancelation once we get that response
        self.cancelPending = YES;
        [self.sipManager cancel];
    }
    
    self.connectionState = ConnectionStateDisconnected;
    self.state = AccountStateReady;

}

- (void) registerSIP:(CDVInvokedUrlCommand*)command {
    
    NSString* username = [[command arguments] objectAtIndex:0];
    NSString* displayName = [[command arguments] objectAtIndex:1];
    NSString* domain = [[command arguments] objectAtIndex:2];
    NSString* password = [[command arguments] objectAtIndex:3];
    NSString* transport = [[command arguments] objectAtIndex:4];
    
    if (self.state == AccountStateOffline) {
        defaultUsername = username;
        defaultDomain = domain;
        defaultPassword = password;
        
        self.isRegistered = NO;
        self.isInitialized = NO;
        self.isTerminatedByCaller = NO;
        self.makeCall = NO;
        
        self.signalingDomain = defaultDomain;
        self.signalingUsername = defaultUsername;
        
        self.parameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                           defaultUsername, @"aor",
                           defaultPassword, @"password",
                           nil];
        [self.parameters setObject:defaultDomain forKey:@"registrar"];
        
        self.sipManager = [[SipManager alloc] initWithDelegate:self params:self.parameters];
        self.sipManager.connectionDelegate = self;
        self.sipManager.deviceDelegate = self;
        [self.sipManager eventLoop];
        [self.sipManager updateParams:self.parameters deviceIsOnline:YES networkIsOnline:YES];
    }
//        else {
//        [self.sipManager updateParams:nil deviceIsOnline:YES networkIsOnline:YES];
//    }
    
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) deregisterSIP:(CDVInvokedUrlCommand*)command {
    NSString* username = [[command arguments] objectAtIndex:0];
    NSString* domain = [[command arguments] objectAtIndex:1];
    
    if (_state != AccountStateOffline) {
        if (_state == AccountStateBusy) {
            [self disconnect];
        }

        self.state = AccountStateOffline;
    }
    
    [self.sipManager shutdown:NO];
    
    
    NSString* message = @"";
    NSString* state = @"RegistrationNone";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       @"REGISTRATION_CHANGE", @"event",
                                       message, @"message",
                                       defaultUsername, @"username",
                                       defaultDomain, @"domain",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
    
    
    CDVPluginResult* directPluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:directPluginResult callbackId:command.callbackId];
}

- (void) getRegisterStatusSIP:(CDVInvokedUrlCommand*)command {
    NSString* username = [[command arguments] objectAtIndex:0];
    NSString* domain = [[command arguments] objectAtIndex:1];
    
    CDVPluginResult* pluginResult = nil;
    // if ([[LinphoneHelper instance] getRegisterStatusSIPWithUsername:username domain:domain]) {
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"RegistrationOk"];
    // } else {
    //     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"RegistrationFailed"];
    // }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) makeCall:(CDVInvokedUrlCommand*)command {
    NSString* username = [[command arguments] objectAtIndex:0];
    NSString* domain = [[command arguments] objectAtIndex:1];
    NSString* displayName = [[command arguments] objectAtIndex:2];
    
    
    self.callUsername = username;
    self.callDomain = domain;
    self.makeCall = YES;
    
    if (self.state == AccountStateOffline) {
        self.isRegistered = NO;
        self.isInitialized = NO;
        self.isTerminatedByCaller = NO;
        self.makeCall = YES;
        
        self.signalingDomain = defaultDomain;
        self.signalingUsername = defaultUsername;
        
        self.parameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                           defaultUsername, @"aor",
                           defaultPassword, @"password",
                           nil];
        [self.parameters setObject:defaultDomain forKey:@"registrar"];
        
        self.sipManager = [[SipManager alloc] initWithDelegate:self params:self.parameters];
        self.sipManager.connectionDelegate = self;
        self.sipManager.deviceDelegate = self;
        [self.sipManager eventLoop];
        [self.sipManager updateParams:self.parameters deviceIsOnline:YES networkIsOnline:YES];
    } else {
        // call the other party
        NSString* rcusername = [[NSString alloc] initWithFormat:@"sip:%@@%@", username, domain];
    
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"fixed_callid", @"CALL-ID",
                                        nil];
        self.cancelPending = NO;
        self.state = AccountStateBusy;
        self.connectionState = ConnectionStatePending;
        if (![self.sipManager invite:rcusername withVideo:NO customHeaders:headers]) {
            self.state = AccountStateReady;
            return;
        }
    
        [[CallManager sharedInstance] startCallWithPhoneNumber:username];
    }

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


- (void) stopListener:(CDVInvokedUrlCommand*)command {
    [NSNotificationCenter.defaultCenter removeObserver:self];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

/**
 *  @abstract Emitted when a connection failed and got disconnected
 *
 *  @param connection Connection that failed
 *  @param error      Description of the error of the Connection
 */
- (void)connectionDidFailWithError:(NSError*)error {
    // nothingr
    NSLog(@"Call failed");
    [[CallManager sharedInstance] endCall];
}

/**
 *  @abstract Emitted when an RCConnection start to connect
 *
 *  @param connection Connection of interest
 */
- (void)connectionDidStartConnecting {
    // nothing
}

/**
 *  @abstract Connection is established
 *
 *  @param connection Connection of interest
 */
- (void)connectionDidConnect {
    // nothing
}

/**
 *  @abstract Incoming connection was cancelled
 *
 *  @param connection Connection of interest
 */
- (void)connectionDidCancel {
    NSString* message = @"";
    NSString* state = @"CallEnd";
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

/**
 *  @abstract Connection was declined from the remote party
 *
 *  @param connection Connection of interest
 */
- (void)connectionDidGetDeclined {
    
    [self.sipManager bye];
    [self.sipManager cancel];
    [self.sipManager disconnectMedia];
//    [SipManager emptyMediaList];
//    [SipManager createMediaList];
    [[CallManager sharedInstance] endCall];
    
    NSString* message = @"";
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       @"CallDecline", @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
    
    resultDict = [ [NSMutableDictionary alloc]
                  initWithObjectsAndKeys :
                  event, @"event",
                  message, @"message",
                  callId, @"callId",
                  caller, @"caller",
                  callee, @"callee",
                  @"connectionDidGetDeclined", @"from",
                  @"STATE", @"state",
                  nil
                  ];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

- (void)sipManagerDidReceiveIncomingCancelled:(SipManager*)sipManager {
    // nothing
}
- (void)sipManager:(SipManager*)sipManager didReceiveLocalVideo:(RTCVideoTrack *)localView {
    
}
- (void)sipManager:(SipManager*)sipManager didReceiveRemoteVideo:(RTCVideoTrack *)remoteView {
    
}
- (void)sipManager:(SipManager*)sipManager didMediaError:(NSError *)error {
    [self.sipManager bye];
    [self.sipManager cancel];
    [self.sipManager disconnectMedia];
//    [SipManager emptyMediaList];
//    [SipManager createMediaList];
    [[CallManager sharedInstance] endCall];
    
    NSString* message = @"";
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       @"CallDecline", @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
    resultDict = [ [NSMutableDictionary alloc]
                  initWithObjectsAndKeys :
                  event, @"event",
                  message, @"message",
                  callId, @"callId",
                  caller, @"caller",
                  callee, @"callee",
                  self.sipManager.media.number, @"number",
                  @"STATE_FAILED", @"state",
                  nil
                  ];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}
- (void)sipManager:(SipManager*)sipManager didSignallingError:(NSError *)error {
    //    [self.sipManager bye];
    //    [self.sipManager cancel];
    [self.sipManager disconnectMedia];
//    [SipManager emptyMediaList];
//    [SipManager createMediaList];
    [[CallManager sharedInstance] endCall];
    [self disconnect];
    
    NSString* message = @"";
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       @"didSignallingError", @"from",
                                       @"STATE", @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

/**
 *  @abstract Connection was disconnected
 *
 *  @param connection Connection of interest
 */
- (void)connectionDidDisconnect {
    
    NSString* message = @"";
    NSString* state = @"Released";
    [[CallManager sharedInstance] endCall];
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

/**
 *  @abstract Received local video track for the connection. To see the video it must be rendered inside a suitable view
 *
 *  @param connection Connection of interest
 *  @param localVideoTrack Local video track
 */
//- (void)connection:(RCConnection *)connection didReceiveLocalVideo:(RTCVideoTrack *)localVideoTrack {
//
//}

/**
 *  @abstract Received remote video track for the connection. To see the video it must be rendered inside a suitable view
 *
 *  @param connection Connection of interest
 *  @param remoteVideoTrack Remote video track
 */
//- (void)connection:(RCConnection *)connection didReceiveRemoteVideo:(RTCVideoTrack *)remoteVideoTrack {
//    int abc = 4;
//}

/**
 *  @abstract RCDevice stopped listening for incoming connections (**Not Implemented yet**)
 *
 *  @param device Device of interest
 *  @param error  The reason it stopped
 */
- (void)deviceDidStopListeningForIncomingConnections:(NSError*)error {
    NSString* message = @"";
    NSString* state = @"RegistrationNone";
    NSString* username = defaultUsername;
    NSString* domain = defaultDomain;
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       @"REGISTRATION_CHANGE", @"event",
                                       message, @"message",
                                       username, @"username",
                                       domain, @"domain",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

/**
 *  @abstract RCDevice started listening for incoming connections
 *
 *  @param device Device of interest
 */
- (void)deviceDidStartListeningForIncomingConnections {
    NSString* message = @"";
    NSString* state = @"";
    //    NSNumber *number = [notif.userInfo objectForKey:@"state"];
    //    LinphoneRegistrationState registrationState = [number integerValue];
    //    if (registrationState == LinphoneRegistrationOk) {
    state = @"RegistrationOk";
    //    } else if (registrationState == LinphoneRegistrationNone) {
    //        state = @"RegistrationNone";
    //    } else if (registrationState == LinphoneRegistrationProgress) {
    //        state = @"RegistrationProgress";
    //    } else if (registrationState == LinphoneRegistrationCleared) {
    //        state = @"RegistrationCleared";
    //    } else if (registrationState == LinphoneRegistrationFailed) {
    //        state = @"RegistrationFailed";
    //    }
    
    
    NSString* username = defaultUsername;
    NSString* domain = defaultDomain;
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       @"REGISTRATION_CHANGE", @"event",
                                       message, @"message",
                                       username, @"username",
                                       domain, @"domain",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

/**
 *  @abstract RCDevice received incoming connection
 *
 *  @param device     Device of interest
 *  @param connection Newly established connection
 */
- (void)deviceDidReceiveIncomingConnection {
    int asdg = 4;
}

/**
 *  @abstract RCDevice received incoming instant message
 *
 *  @param device  Device of interest
 *  @param message Instant message text
 *  @param params Dictionary with parameters of the incoming message
 */
//- (void)deviceDidReceiveIncomingMessage:(NSString *)message withParams:(NSDictionary *)params {
//    int asdg = 4;
//}

//- (void)deviceDidReceivePresenceUpdate:(RCPresenceEvent *)presenceEvent {
//    int asdg = 4;
//}


- (void) startListener:(CDVInvokedUrlCommand*)command {
    
    listenCallbackId = command.callbackId;
    
    [NSNotificationCenter.defaultCenter removeObserver:self];
    
    
    
    // [[NSNotificationCenter defaultCenter] addObserverForName:kLinphoneCallUpdate object:nil queue:nil usingBlock:^(NSNotification * _Nonnull notif) {
    
    
    
    // }];
    
    
}

- (void)sipManagerDidReceiveOutgoingRinging:(SipManager*)sipManager {
    if (self.cancelPending) {
        [self.sipManager cancel];
        return;
    }
    self.connectionState = ConnectionStateConnecting;
}

- (void)sipManagerDidReceiveOutgoingCancelled:(SipManager*)sipManager { // we cancel
    [self.sipManager bye];
    [self.sipManager cancel];
    [self.sipManager disconnectMedia];
//    [SipManager emptyMediaList];
//    [SipManager createMediaList];
    [[CallManager sharedInstance] endCall];
}

- (void)sipManagerDidReceiveOutgoingDeclined:(SipManager*)sipManager {
    [self.sipManager bye];
    [self.sipManager cancel];
    [self.sipManager disconnectMedia];
//    [SipManager emptyMediaList];
//    [SipManager createMediaList];
    [[CallManager sharedInstance] endCall];
    
    NSString* message = @"";
    NSString* state = @"CallDecline";
    
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
    resultDict = [ [NSMutableDictionary alloc]
                  initWithObjectsAndKeys :
                  event, @"event",
                  message, @"message",
                  callId, @"callId",
                  caller, @"caller",
                  callee, @"callee",
                  @"sipManagerDidReceiveOutgoingDeclined", @"from",
                  @"STATE", @"state",
                  nil
                  ];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

- (void)sipManagerDidReceiveOutgoingAccepted:(SipManager*)sipManager {
    NSString* message = @"";
    NSString* state = @"Connected";
    
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

- (void)sipManagerDidReceiveBye:(SipManager*)sipManager {
    if (self.isTerminatedByCaller) {
        self.isTerminatedByCaller = NO;
        return;
    } else {
       //    [self.sipManager bye];
       //    [self.sipManager cancel];
       [self.sipManager disconnectMedia];
   //    [SipManager emptyMediaList];
   //    [SipManager createMediaList];
       [[CallManager sharedInstance] endCall];
        
        if (_state != AccountStateOffline) {
            if (_state == AccountStateBusy) {
                [self disconnect];
            }

            self.state = AccountStateOffline;
        }
        
        [self.sipManager shutdown:NO];
   }
    NSString* message = @"";
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       @"CallDecline", @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
    resultDict = [ [NSMutableDictionary alloc]
                  initWithObjectsAndKeys :
                  event, @"event",
                  message, @"message",
                  callId, @"callId",
                  caller, @"caller",
                  callee, @"callee",
                  @"sipManagerDidReceiveBye", @"from",
                  @"STATE", @"state",
                  nil
                  ];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

- (void)sipManagerReadyMedia:(SipManager *)sipManager number:(int)number {
    NSString* message = @"";
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       number, @"number",
                                       @"STATE_DONE", @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

- (void)sipManagerDidReceiveOutgoingEstablished:(SipManager*)sipManager {
    
    if (self.cancelPending) {
        // if the cancel when we got the ringing didn't make it on time
        // then we will have received a 200 OK established. In that case
        // we need to terminate the call with a BYE
        //NSLog(@"[RCConnection outgoingEstablished:bye]");
        [self.sipManager bye];
        [self.sipManager cancel];
        self.cancelPending = NO;
        return;
    }
    
    self.connectionState = ConnectionStateConnected;
    
    NSString* message = @"";
    NSString* state = @"OutgoingInit";
    
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

- (void)sipManager:(SipManager *)sipManager didReceiveMessageWithData:(NSString *)message from:(NSString*)from {
    // nothing
}
// 'ringing' for incoming connections
- (void)sipManagerDidReceiveCall:(SipManager *)sipManager from:(NSString*)from {
    // nothing
}
- (void)sipManagerDidRegisterSuccessfully:(SipManager *)sipManager {
    NSString* message = @"";
    NSString* state = @"";
    
    state = @"RegistrationOk";
    
    self.state = AccountStateReady;
    NSString* username = defaultUsername;
    NSString* domain = defaultDomain;
    
    if (self.makeCall) {
        self.makeCall = NO;
        // call the other party
        NSString* rcusername = [[NSString alloc] initWithFormat:@"sip:%@@%@", self.callUsername, self.callDomain];
    
        NSMutableDictionary *headers = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        @"fixed_callid", @"CALL-ID",
                                        nil];
        self.cancelPending = NO;
        self.state = AccountStateBusy;
        self.connectionState = ConnectionStatePending;
        if (![self.sipManager invite:rcusername withVideo:NO customHeaders:headers]) {
            self.state = AccountStateReady;
            return;
        }
    
        [[CallManager sharedInstance] startCallWithPhoneNumber:self.callUsername];
    }
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       @"REGISTRATION_CHANGE", @"event",
                                       message, @"message",
                                       username, @"username",
                                       domain, @"domain",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}
- (void)sipManagerWillUnregister:(SipManager *)sipManager {
    NSString* message = @"";
    NSString* state = @"";
    
    state = @"RegistrationFailed";
    self.state = AccountStateOffline;
    
    NSString* username = defaultUsername;
    NSString* domain = defaultDomain;
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       @"REGISTRATION_CHANGE", @"event",
                                       message, @"message",
                                       username, @"username",
                                       domain, @"domain",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

- (void)sipManagerSignal:(SipManager *)sipManager signal:(NSString*)signal {
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       @"SIGNAL_SHOW", @"event",
                                       signal, @"signal",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

// fires when signaling facilities are initialized (either registrar-less mode or not)
- (void)sipManagerDidInitializedSignalling:(SipManager *)sipManager {
    NSString* message = @"";
    NSString* state = @"";
    
    state = @"RegistrationProgress";
    
    
    NSString* username = defaultUsername;
    NSString* domain = defaultDomain;
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       @"REGISTRATION_CHANGE", @"event",
                                       message, @"message",
                                       username, @"username",
                                       domain, @"domain",
                                       state, @"state",
                                       nil
                                       ];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}


#pragma mark - Obtaining information about a running call: sound volumes, quality indicators Functions

- (void) terminateCall:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* pluginResult;
    
    self.isTerminatedByCaller = YES;
    
    [self.sipManager bye];
    [self.sipManager cancel];
    [self.sipManager disconnectMedia];
//    [SipManager emptyMediaList];
//    [SipManager createMediaList];
    [[CallManager sharedInstance] endCall];
    
    if (_state != AccountStateOffline) {
        if (_state == AccountStateBusy) {
            [self disconnect];
        }

        self.state = AccountStateOffline;
    }
    
    [self.sipManager shutdown:NO];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
    NSString* caller = @"";
    NSString* callee = @"";
    NSString* callId = @"";
    NSString* message = @"";
    NSString* event = @"CALL_EVENT";
    
    NSMutableDictionary *resultDict = [ [NSMutableDictionary alloc]
                                       initWithObjectsAndKeys :
                                       event, @"event",
                                       message, @"message",
                                       callId, @"callId",
                                       caller, @"caller",
                                       callee, @"callee",
                                       @"terminateCallWithCallId", @"from",
                                       @"STATE", @"state",
                                       nil
                                       ];
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDict];
    [pluginResult setKeepCallback:[NSNumber numberWithBool:YES]];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:listenCallbackId];
}

- (void) terminateCallWithCallId:(CDVInvokedUrlCommand*)command {
    NSString* callId = [[command arguments] objectAtIndex:0];
    CDVPluginResult* pluginResult;
    // LinphoneCall *call = [[LinphoneHelper instance] getLinphoneCallFromCallId:callId];
    // if (call) {
    // [[LinphoneHelper instance] terminateCall:call];
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    // } else {
    //     pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Call ID not found."];
    // }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
    
}

- (void) muteCall:(CDVInvokedUrlCommand*)command {
    //    [self.connection setMuted:YES];
    [self.sipManager setMuted:YES];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
- (void) unmuteCall:(CDVInvokedUrlCommand*)command {
    //    [self.connection setMuted:NO];
    [self.sipManager setMuted:NO];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (bool)allowSpeaker {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return true;
    }
    
    
    bool allow = true;
    CFStringRef lNewRoute = CFSTR("Unknown");
    UInt32 lNewRouteSize = sizeof(lNewRoute);
    OSStatus lStatus = AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &lNewRouteSize, &lNewRoute);
    if (!lStatus && lNewRouteSize > 0) {
        NSString *route = (__bridge NSString *)lNewRoute;
        allow = ![route containsSubstring:@"Heads"] && ![route isEqualToString:@"Lineout"];
        CFRelease(lNewRoute);
    }
    return allow;
}

- (void) setEnableSpeaker:(bool)enable {
    OSStatus ret;
    _speakerEnabled = enable;
    UInt32 override = kAudioSessionUnspecifiedError;
    
    if (!enable && _bluetoothAvailable) {
        UInt32 bluetoothInputOverride = _bluetoothEnabled;
        ret = AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryEnableBluetoothInput,
                                      sizeof(bluetoothInputOverride), &bluetoothInputOverride);
        // if setting bluetooth failed, it must be because the device is not available
        // anymore (disconnected), so deactivate bluetooth.
        if (ret != kAudioSessionNoError) {
            _bluetoothAvailable = _bluetoothEnabled = FALSE;
        }
    }
    
    if (override != kAudioSessionNoError) {
        if (enable && [self allowSpeaker]) {
            override = kAudioSessionOverrideAudioRoute_Speaker;
            ret = AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(override), &override);
            _bluetoothEnabled = FALSE;
        } else {
            override = kAudioSessionOverrideAudioRoute_None;
            ret = AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(override), &override);
        }
    }
    
    if (ret != kAudioSessionNoError) {
        NSLog(@"Failed to change audio route: err %d", ret);
    }
}

- (void) enableSpeaker:(CDVInvokedUrlCommand*)command {
    // [[LinphoneHelper instance] enableSpeaker];
    [self setEnableSpeaker:YES];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) disableSpeaker:(CDVInvokedUrlCommand*)command {
    // [[LinphoneHelper instance] disableSpeaker];
    [self setEnableSpeaker:NO];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) getCallInformation:(CDVInvokedUrlCommand*)command {
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) sendMessageWithChatRoom:(CDVInvokedUrlCommand*)command {
    NSString* username = [[command arguments] objectAtIndex:0];
    NSString* message = [[command arguments] objectAtIndex:1];
    
    //    [parameters setValue:@"abcd" forKey:@"call-id"];
    
    //NSString* uri = [NSString stringWithFormat:[parameters objectForKey:@"uas-uri-template"], [parameters objectForKey:@"username"]];
    [self.sipManager message:message to:username customHeaders:nil];
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


@end
